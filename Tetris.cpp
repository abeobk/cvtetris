#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

const int board_width = 15;
const int board_height = 2 * board_width;
const int block_size = 30;
const int game_speed = 20;

Mat board(board_height, board_width, CV_8UC1);
Mat tmp_board = board.clone();

uchar blocks[][5] = {
    {0x04,0x04,0x04,0x04,0x00},//4x1
    {0x00,0x04,0x0E,0x00,0x00},//T
    {0x00,0x0C,0x04,0x04,0x00},//LL
    {0x00,0x06,0x04,0x04,0x00},//RL
    {0x00,0x0C,0x06,0x00,0x00},//L~
    {0x00,0x06,0x0C,0x00,0x00},//R~
    {0x00,0x0C,0x0C,0x00,0x00},//2x2
    //{0x00,0x0C,0x04,0x06,0x00},//Z
    //{0x00,0x06,0x04,0x0C,0x00},//RZ
    //{0x00,0x04,0x04,0x04,0x00},//3x1
    //{0x00,0x04,0x04,0x00,0x00},//2x1
    //{0x00,0x00,0x04,0x00,0x00},//1x1
    //{0x00,0x04,0x0E,0x04,0x00},//+
    //{0x00,0x0E,0x04,0x04,0x00},//LT
    //{0x0C,0x04,0x04,0x06,0x00},//Z
    //{0x06,0x04,0x04,0x0C,0x00},//RZ
    //{0x00,0x0C,0x0C,0x0C,0x00},//3x2
    //{0x00,0x0E,0x0E,0x0E,0x00},//3x3
    //{0x00,0x0A,0x0E,0x00,0x00},//2x2
};
#define BLOCK_COUNT (sizeof(blocks)/5)

int crr_block_idx = 0;
Point crr_center = {5,0};
Mat crr_block(5, 5, CV_8UC1);

int frm_idx = 0;
int my_score = 0;

bool movable(){
    for(int r = 0; r < 5; r++){
        for(int c = 0; c < 5; c++){
            int x = crr_center.x + c - 2;
            int y = crr_center.y + r - 2;
            if(y < 0)continue;
            if(crr_block.at<uchar>(r, c)){
                if(x < 0 || x >= board.cols || y >= board.rows || board.at<uchar>(y, x))return false;
            }
        }
    }
    return true;
}

void rotate(){
    Mat tmp_block = crr_block.clone();
    rotate(crr_block, crr_block, ROTATE_90_CLOCKWISE);
    if(!movable()) crr_block = tmp_block.clone();
}
void move_left(){
    crr_center.x--;
    if(!movable())crr_center.x++;
}
void move_right(){
    crr_center.x++;
    if(!movable())crr_center.x--;
}
void move_down(){
    crr_center.y++;
    if(!movable())crr_center.y--;
}

void gen_new_block();
//setup the board
void setup(){
    //clear board
    board = 0;
    my_score = 0;
    gen_new_block();
}

//Generate new block
void gen_new_block(){
    crr_block_idx = rand() % BLOCK_COUNT;
    cout << "Current block id: " << crr_block_idx << endl;
    int color = (rand() % 18) * 10 + 10;
    for(int r = 0; r < 5; r++){
        uchar rowdata = blocks[crr_block_idx][r];
        for(int c = 0; c < 5; c++){
            crr_block.at<uchar>(r, 4 - c) = ((rowdata >> c) & 1) ? color : 0;
        }
    }
    //rotate(crr_block, crr_block, ROTATE_90_CLOCKWISE);
    crr_center = {board.cols / 2, 0};
    if(!movable()){
        cout << "Game over!" << endl;
        system("pause");
        setup();
    }
}

//draw a block
void draw_block(Mat& brd, const Mat& blk){
    for(int r = 0; r < 5; r++){
        for(int c = 0; c < 5; c++){
            int x = crr_center.x + c - 2;
            int y = crr_center.y + r - 2;
            if(x < 0 || y < 0 || x >= brd.cols || y >= brd.rows || !blk.at<uchar>(r, c))continue;
            brd.at<uchar>(y, x) = blk.at<uchar>(r, c);
        }
    }
}

//draw function
void render(int block_size){
    tmp_board = board.clone();
    draw_block(tmp_board, crr_block);
    //render
    Mat img;
    resize(tmp_board, img, tmp_board.size()*block_size, 0, 0, INTER_NEAREST);
    //convert to HSV
    merge(std::vector<Mat>{ img, img > 0, img > 0 }, img);
    cvtColor(img, img, COLOR_HSV2RGB);
    imshow("ABEO TETRIS", img);
}

//shift the board down
void shift_board_down(int row){
    for(int r = row; r > 0; r--){
        for(int c = 0; c < board.cols; c++)
            board.at<uchar>(r, c) = board.at<uchar>(r - 1, c);
    }
    //make sure top row is empty
    line(board, {0,0}, {board.cols,0}, {0,0,0}, 1);
}

//delete a row
void delete_row(int row){
    //animate
    for(int c = 0; c < board.cols; c++){
        board.at<uchar>(row, c) = 0;
        render(block_size);
        waitKey(10);
    }
}

//calculate score
void score(){
    int bonus = 0;
    for(int r = 0; r < board.rows; r++){
        int cnt = 0;
        for(int c = 0; c < board.cols; c++){
            if(board.at<uchar>(r, c))cnt++;
        }
        if(cnt == board.cols){
            bonus++;
            delete_row(r);
            //shift board
            shift_board_down(r);
            render(block_size);
            waitKey(30);
        }
    }
    my_score += bonus << 1;
    cout << "SCORE: " << my_score << endl;
}

//skip
void skip(){
    frm_idx = game_speed - 1;
}

//update
void update(){
    //movable?
    frm_idx++;
    if(frm_idx == game_speed){
        crr_center.y++;
        if(!movable()){
            crr_center.y--;
            draw_block(board, crr_block);
            crr_block = 0;
            score();
            gen_new_block();
        }
        frm_idx = 0;
    }
}

//Main game loop
void game_loop(){
    for(;;){
        update();
        render(block_size);
        char key = waitKey(5);
        if(key == 'q')break;
        else if(key == 'w'){ rotate(); } else if(key == 'a'){ move_left(); } else if(key == 'd'){ move_right(); } else if(key == 's'){ move_down(); skip(); }
    }
}



//Main function
int main(){
    cout << "Abeo Tetris" << endl;
    setup();
    game_loop();
}
